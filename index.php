<?php
$user_id = (isset($_GET['user_id'])) ? $_GET['user_id'] : "";
$estudio = (isset($_GET['estudio'])) ? $_GET['estudio'] : "";
?>
<!DOCTYPE html>
<html>
    <head lang="es">
        <meta charset="utf-8">
        <title>Webcam Boutique</title>
        <meta name="viewport" content="width=divece-width,user-scalabre=no, initial-scale=1.0,maximum-scale=1.0, minimun-scale=1.0">
        <link rel="prefetch" href="img/diego.png">
        <link rel="stylesheet" type="text/css" href="fonts/iconic/css/material-design-iconic-font.min.css">
        <link type="text/css" rel="stylesheet" href="css/font-awesome.min.css">    
        <link type="text/css" rel="stylesheet" href="css/bootstrap.min.css">
        <link type="text/css" rel="stylesheet" href="css/main.css">
        <link type="text/css" rel="stylesheet" href="css/carrito.css">
        <link id="media" type="text/css" rel="stylesheet" media="all" href="">
        <link type="text/css" rel="stylesheet" href="css/loader.css">
        <link type="image/ico" rel="icon" href="img/bolsa.ico"> 
    </head>
    <body>
    <input type="hidden" id="user_id" value="<?php echo str_replace('"',"",$user_id); ?>">
    <input type="hidden" id="estudio" value="<?php echo str_replace('"',"",$estudio); ?>">
<!--
    ===============================================================================================================================
                                                    PRELOADER
    ===============================================================================================================================
 -->  
    <div id="preloader" class="preloader">
        <div id="loader">
            <div class="line-scale-pulse-out">
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
            </div>
        </div>
    </div>
    <!---===========================================================  HEADER  BEGIN ========================================================--->
    <div class="content-header">            
        <header id="header">
                <nav>
                    <nav class="menu">
                            <div class="logo">
                                <a href="index.php"><img class="imgLogo" src="img/logo2.png"></a>
                                <a href="#" class="btn-menu" id="btn-menu"><i class="fa fa-bars" ></i></a>
                            </div>
                            
                            <div class="enc enlaces">
                                <a id="tienda" class="nav-item" >Tienda</a>
                            </div>
                            <div id="enlaces" >
                                <div class="enlaces"><a id="tienda2" class="nav-item">Tienda</a></div>
                                <div id="categoryGenerate" class="enlaces categoryGenerate"></div>
                                    <div class="enlaces " >
                                        <a class="nav-item" id="support">Soporte</a>
                                        <a class="nav-item search-open" ><i class="fa fa-search"></i></a>
                                        <a class="nav-item" >
                                            <i id="carrito"  class="fa fa-shopping-cart"></i>
                                        </a>
                                </div>
                            </div>      
                        </nav>
                </nav>
        </header>
        <div class="search-inline">
                <form action="#">
                    <input onkeyup="setSearchProduct(this.value);" type="text" class="form-control input_search" placeholder="Buscar...">
                   
                    <a href="javascript:void(0)" class="search-close">
                        <i class="fa fa-times"></i>
                    </a>
                </form>
        </div>
    </div>
    <!---===========================================================  HEADER END ========================================================--->
<!------------------------------------------------------------------------------------------------------------------------------------------->
    <!---===========================================================  SHOW CAR  BEGIN ========================================================--->
    <div id="show_carrito" class="wrap-header-cart js-panel-cart ">
        <div class="s-full js-hide-cart"></div>

        <div class="header-cart flex-col-l p-l-65 p-r-25">
            <div class="header-cart-title flex-w flex-sb-m p-b-8">
                <span class="mtext-103 cl2">
                                Carrito
                            </span>

                <div class="fs-35 lh-10 cl2 p-lr-5 pointer hov-cl1 trans-04 js-hide-cart">
                    <i id='close_carrito' class="zmdi zmdi-close"></i>
                </div>
            </div>

            <div class="header-cart-content flex-w js-pscroll">
                <ul class="header-cart-wrapitem w-full barra_carrito" id="ul_car">

                </ul>
                <div class="w-full">
                    <div class="header-cart-total w-full p-tb-40" id="total_cart">

                    </div>

                    <div class="header-cart-buttons flex-w w-full">
                        <a href="#" id="show-cat-btn" class="flex-c-m stext-101 cl0 size-107 bg3 bor2 hov-btn3 p-lr-15 trans-04 m-r-8 m-b-10">
                                        Ver Carrito
                                    </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Cart -->
    <!---===========================================================  SHOW CAR END ========================================================--->
<!---===========================================================  MAIN DIV  BEGIN ========================================================--->
<div id="principal" class="main">
    <div class="postion_img_main">
        <div ><img  class="main_img" src="img/diego.png" ></div>
    </div>
    
    <div class="position_img_right">                
        <div class="imgRight1" >
            <a class="buy_now_right" id="btn_right_first">Comprar ahora</a>
        </div>
        <div class="imgRight2" >
            <a class="buy_now_right" id="btn_right_second">Comprar ahora</a>
        </div>
    </div>
    <div class="main_content_position">
        <div class="main_tittle">
            <h1>
                DISE&Ntilde;OS EXCLUSIVOS PARA MUJERES EXCLUSIVAS.
            </h1>
        </div>
        <div class="main_button">
            <button class="btn_main" id="btn_main">Comprar Ahora</button>
        </div>
    </div>
</div>
<div id="mostSold" class="most_sold_content">
    <div class="tittle_most_sold">
        <h1>M&Aacute;S VENDIDOS</h1>
    </div>
    <div class="most_sold_items">
        <div class="col-lg-4 col-md-4 col-xs-12 col-sm-12 items_star" >
                <img class="img_most_sold" src="img/Dildo.png" >
                <h6 class="most_sold_name" ><button class="btn_most_sold" data-value="Dildos" onclick="modalDetalleProducto(270);">Doble Dildo</button></h6>
        </div>
        <div class="col-lg-4 col-md-4 col-xs-12 col-sm-12 items_star" >
                <img class="img_most_sold img_most_sold_mid" src="img/DSC04339.png" >
                <h6 class="most_sold_name" ><button class="btn_most_sold" data-value="Lubricantes" onclick="modalDetalleProducto(110);">Lubricante Passion</button></h6>
        </div>
        <div class="col-lg-4 col-md-4 col-xs-12 col-sm-12 items_star" >
            <img class="img_most_sold" src="img/cosme1.png" >
            <h6 class="most_sold_name" ><button class="btn_most_sold" data-value="Cosmeticos" onclick="modalDetalleProducto(567);">Vitaminas Para El Cabello</button></h6>
        </div>
    </div>
</div>
<div id="new" class="news_products_content">
    <div class="new_produc_main">
            <div class="button_new_product">
                <button id="btn_new_prod" data-value="Conjuntos" class="btn_new_prod">Comprar Ahora</button>
            </div>
    </div>
        </div>

<!---===========================================================  MAIN DIV END ===========================================================--->

        <!------------------------------------------------------------------------------------------------------------------------------------------->

        <!-- SECTION ALL PRODUCTS-->
    <div id="sectionProductos" class="allProducts" data-value="1">
        <!-- container -->
        <div  id="contProd" style="z-index: 0;">
            <!-- row -->
            <div class="rower">

                <!-- section title -->
                <div class="col-md-12">
                    <div class="section-title">
                        <h3 class="title">Productos</h3>
                        <div id="categorys_most_sold" class="line_buttons_category">

                        </div>
                    </div>
                </div>
                <!-- /section title -->
                <!-- Products tab & slick -->
                <div class="col-md-12 " >
                    <div class="rower pts_tarjet">
                        <div class="products-tabs">
                            <!-- tab -->
                            <div id="tab1" class="divAllProducts row" data-nav="#slick-nav-1" >
                                   
                            </div>
                            <!-- /tab -->
                        </div>
                    </div>
                </div>
                <!-- Products tab & slick -->
                 <!-- Products tab & slick -->
                 <div class="col-md-12 " >
                    <div class="row pts_tarjet">
                        <div class="products-tabs">
                            <!-- tab -->
                            <div id="list_product" style="display:none;" class="block_category" data-nav="#slick-nav-1" >
                                   
                            </div>
                            <!-- /tab -->
                        </div>
                    </div>
                </div>
                <!-- Products tab & slick -->
                
            </div>    
            <!-- /row -->
        </div>
        <!-- /container -->
    </div>
    <!-- /SECTION ALL PRODUCTS-->   
<!------------------------------------------------------------------------------------------------------------------------------------------->   

<!--------------------------------------------------------------- MODAL BEGIN---------------------------------------------------------------------------->
	<div class="wrap-modal1 js-modal1 p-t-60 p-b-20">
		<div class="overlay-modal1 js-hide-modal1"></div>

		<div class="container">
			<div class="bg0 p-t-60 p-b-30 p-lr-15-lg how-pos3-parent">
				<button class="how-pos3 hov3 trans-04 js-hide-modal1">
                    <img src="img/icons/icon-close.png" alt="CLOSE">
                </button>

				<div class="row">
					<div class="col-md-6 col-lg-7 p-b-30">
						<div class="p-l-25 p-r-30 p-lr-0-lg">
							<div class="wrap-slick3 flex-sb flex-w">
								<div class="wrap-slick3-dots"></div>
								<div class="wrap-slick3-arrows flex-sb-m flex-w"></div>

								<div class="slick3 gallery-lb">
									<div class="item-slick3" id="modal_img_thumb" data-thumb="">
										<div class="wrap-pic-w pos-relative">
											<img src="" id="modal_img_detail" alt="IMG-PRODUCT">

											
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					
					<div class="col-md-6 col-lg-5 p-b-30">
						<div class="p-r-50 p-t-5 p-lr-0-lg">
							<h1 class="mtext-105 cl2 js-name-detail p-b-14" id="modal_img_nombre">
							</h1>

							<span class="mtext-106 cl2" id="cart_price_">
							</span>

							<!--  -->
							<div class="p-t-33">
								<div class="flex-w flex-r-m p-b-10">
									<div class="size-204 flex-w flex-m respon6-next" id="action-addCart">
										<div class="wrap-num-product flex-w m-r-20 m-tb-10">
											<div class="btn-num-product-down cl8 hov-btn3 trans-04 flex-c-m">
												<i class="fs-16 zmdi zmdi-minus">-</i>
											</div>

											<input class="mtext-104 cl3 txt-center num-product" id="cart_quantity_input_" type="number" name="num-product" value="1">

											<div class="btn-num-product-up cl8 hov-btn3 trans-04 flex-c-m">
												<i class="fs-16 zmdi zmdi-plus"></i>
											</div>
										</div>

										<button type="button" class="btn_buy_modal flex-c-m stext-101 cl0 size-101 bg1 bor1 hov-btn1 p-lr-15 trans-04 js-addcart-detail">
										</button>
									</div>
								</div>	
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
    </div>
<!--------------------------------------------------------------- MODAL END---------------------------------------------------------------------------->
<!--======================================LISTA DE COMPRA BEGGIN========================================--->
<section id="cart_items">
        <div class="">
            <!--   Big container   -->
            <div class="container">
                <div class="">
                    <div class="col-md-12">
                        <!--      Wizard container        -->
                        <div class="wizard-container">
                            <div class="card wizard-card" data-color="azzure" id="wizard">
                                <form method="POST" id="form-comprar">
                                    <!--        You can switch ' data-color="azzure" '  with one of the next bright colors: "blue", "green", "orange", "red"          -->

                                    <div class="wizard-header">
                                        <h3>
                                            <b>LISTA DE COMPRA  </b><br>
                                            <small>Esta información nos permitirá saber más sobre usted y los productos que desea.</small>
                                        </h3>
                                    </div>
                                    <div class="wizard-navigation">
                                        <ul>
                                            <li><a class="aCarrito" href="#details" data-toggle="tab">Detalle del carrito</a></li>
                                            <li><a id="infoPersonal" class="aCarrito" href="#captain" data-toggle="tab">Información Personal</a></li>
                                        </ul>
                                    </div>
                                    <div class="tab-content">
                                        <div class="tab-pane" id="details">
                                            <div class="col-md-12" id="content_product">

                                            </div>
                                        </div>
                                        <div class="tab-pane" id="captain">
                                            <h4 class="info-text" >Información Personal</h4>
                                            <div class="col-md-12 formulario row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="full_name">Nombre completo <small>(required)</small></label>
                                                        <input type="text" class="form-control" id="full_name" placeholder="Nombre completo" name="full_name" required value="">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="phone_contact">Telefono de contacto <small>(required)</small></label>
                                                        <input type="text" class="form-control" id="phone_contact" placeholder="Telefono de contacto" name="phone_contact" required value="">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="email">Email <small>(required)</small></label>
                                                        <input type="email" class="form-control" id="email" placeholder="Email" name="email" required value="">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="location">Locación <small>(required)</small></label>
                                                        <input type="text" class="form-control" id="location" placeholder="Cali, Valle del cauca" name="location" required value="">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="address">Dirección <small>(required)</small></label>
                                                        <input type="text" class="form-control" id="address" placeholder="Dirección" name="address" required value="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="description">
                                            <div class="col-md-12" id="content_product_detail">

                                            </div>
                                            <input type="hidden" name="bought" id="bought" value="">
                                            <input type="hidden" name="locacion" id="name_locacion" value="">
                                            <input type="hidden" name="estudio" id="name_estudio" value="">
                                            <input type="hidden" name="rol" id="rol" value="">
                                        </div>
                                    </div>

                                    <div class="wizard-footer">
                                        <div class="pull-right">
                                            <input type='button' class='btn btn-next btn-fill btn-info btn-wd btn-sm' name='next' id="siguiente" value='Siguiente' />
                                            <input type='button' class='btn btn-finish btn-fill btn-info btn-wd btn-sm' name='finish' id="comprar" value='Comprar!' />
                                        </div>
                                        <div class="pull-left">
                                            <input type='button' id="volver" class='btn btn-previous btn-fill btn-default btn-wd btn-sm' name='previous' value='Volver' />
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- wizard container -->
                    </div>
                </div>
                <!-- row -->
            </div>
            <!--  big container -->
        </div>
</section>
<!--======================================LISTA DE COMPRA END========================================--->

<!---===========================================================  SOPORTE BEGIN ========================================================--->
<section id="section_support" class="support">
    <div class="container">
        <div class="row">
            <div class="div_formulario_email">
                <h3 class="tittle_support">Contactanos</h3> 
                <form method="post" action="" class="formulario_email">
                    <label> Nombre completo: </label>
                    <input id="nombre" type="text" onblur="validador(this);enableFuntion(this);" class="form-control" placeholder="Nombre completo" name="nombre" require>
                    <div id="nombreFeedBack" class="feedBack">Este campo no puede contener caracteres especiales y debe de completarse.</div>
                    <label> Asunto: </label>
                    <input id="asunto" type="text" onblur="validador(this);enableFuntion(this);" class="form-control" placeholder="Asunto" name="asunto" require>
                    <div id="asuntoFeedBack" class="feedBack">Este campo no puede contener caracteres especiales y debe de completarse.</div>
                    <label> Correo electronico: </label>
                    <input id="email_soporte" type="email" onblur="validadorEmail(this);enableFuntion(this);" class="form-control" placeholder="Correo electronico" name="email" require>
                    <div id="emailFeedBack" class="feedBack">Este campo no puede contener caracteres especiales y debe de completarse.</div>
                    <label> Descripcion: </label>
                    <textarea id="desc" class="form-control form_description" onblur="validador(this);enableFuntion(this);" placeholder="Escriba..." name="descripcion" require></textarea>
                    <div id="descFeedBack" class="feedBack">Este campo no puede contener caracteres especiales y debe de completarse.</div>
                    <button id="btnSendEmail" type="submit" class="btn btn-md btn-sm btn-xs btn_send_form" disabled="true">Enviar</button>
                </form>
            </div>
        </div>
    </div>        
</section>
<!---===========================================================  SOPORTE END ========================================================--->
<!---===========================================================  FOOTER  BEGIN ========================================================--->
<div class="footer">
    <div class="tittle_footer" >
        <h6>&copy; WebCamBoutique 2019</h6>
    </div>
</div>
<!---===========================================================  FOOTER  END ========================================================--->
    </body>
    <footer>
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/jquery.validate.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/loader.js"></script>
    <script src="js/jquery.bootstrap.wizard.js"></script>
    <script src="js/gsdk-bootstrap-wizard.js"></script>
    <script src="js/main.js"></script>
    <script src="js/device-uuid.js"></script>
    <script src="js/util.js"></script>
    <script src="js/pace.min.js"></script>
    <script src="js/cart.js"></script>
    <script src="js/sweetalert.min.js"></script>

    <script>
        function validador(campo){        
            if(validando(campo.value)!=0 || campo.value==""){
                $("#"+campo.id).addClass("is-invalid");
                $("#"+campo.id).removeClass("is-valid");
                $("#"+campo.id+"FeedBack").addClass("invalid-feedback");
                $("#"+campo.id+"FeedBack").css("display","block");
            }
            else{
                $("#"+campo.id).addClass("is-valid");
                $("#"+campo.id).removeClass("is-invalid");
                $("#"+campo.id+"FeedBack").addClass("valid-feedback");
                $("#"+campo.id+"FeedBack").css("display","none");
            }
        }

        function validadorEmail(campo){        
            if(validandoEmail(campo.value)!=0){
                $("#email_soporte").addClass("is-invalid");
                $("#email_soporte").removeClass("is-valid");
                $("#email_soporte"+"FeedBack").addClass("invalid-feedback");
                $("#email_soporte"+"FeedBack").css("display","block");
            }
            else{
                document.getElementById("email_soporte").className = "form-control is-valid";
                $("#email"+"FeedBack").addClass("valid-feedback");
                $("#email"+"FeedBack").css("display","none");
            }
        }

        function validando(campo){
            var valorElemento = campo;
            var comilla = '"';
            var comillas = '""';
            var cont = 0;
            for (var i = 0; i < valorElemento.length; i++) {
                if (valorElemento[i] == comilla ||
                        valorElemento[i] == comillas ||
                        valorElemento[i] == "'" ||
                        valorElemento[i] == "(" ||
                        valorElemento[i] == ")" ||
                        valorElemento[i] == "*" ||
                        valorElemento[i] == "/" ||
                        valorElemento[i] == "=" ||
                        valorElemento[i] == "%" ||
                        valorElemento[i] == "$" ||
                        valorElemento[i] == "#" ||
                        valorElemento[i] == "." ||
                        valorElemento[i] == "_" ||
                        valorElemento[i] == "^" ||
                        valorElemento[i] == "{" ||
                        valorElemento[i] == "}" ||
                        valorElemento[i] == "[" ||
                        valorElemento[i] == "]" ||
                        valorElemento[i] == ";" ||
                        valorElemento[i] == ":" ||
                        valorElemento[i] == "," ||
                        valorElemento[i] == "+" ||
                        valorElemento[i] == "¡" ||
                        valorElemento[i] == "!" ||
                        valorElemento[i] == "°" ||
                        valorElemento[i] == "|" ||
                        valorElemento[i] == "@" 
                ) {
                        cont++;
                }
            }
            return cont;
        }

        function validandoEmail(campo){
            var cont = 1;
            if (/^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/.test(campo)){
                cont--;
            } else {
                cont=1;
            }
            return cont;
        }

        var contValidaClass=0;

        function enableFuntion(campo){
            if(campo.getAttribute("class")=="form-control is-valid" || campo.getAttribute("class")=="form-control form_description is-valid"){
                contValidaClass++;
            }
            else{
                contValidaClass--;
            }

            if(contValidaClass>=4){
                $("#btnSendEmail").removeAttr("disabled");
            }
            else{
                $("#btnSendEmail").attr({"disabled":" "});
            }
        }

        var sp = document.querySelector('.search-open');
        var searchbar = document.querySelector('.search-inline');
        var shclose = document.querySelector('.search-close');

        function changeClass() {
            searchbar.classList.add('search-visible');
        }
        function closesearch() {
            searchbar.classList.remove('search-visible');
        }

        sp.addEventListener('click', changeClass);
        shclose.addEventListener('click', closesearch);
        
       function linkInNav(e){
            if($(window).width()>=900){
                var id="";
                id=e.getAttribute("data-value");
                for(i=0;i<$(".btnsLinks").length;i++){                
                    if(id==$(".btnsLinks")[i].id){
                        $(".btnsLinks")[i].click();
                    }
                }
            }
            else{
                if(e.getAttribute("data-value")!=""){
                    setProductoCategoria(e.getAttribute("data-value"));
                    $("#btn-menu").click();
                }                
                else{
                    setAllProductos();
                    $("#btn-menu").click();
                }
            }
            $("#principal").css("display","none");
            $("#mostSold").css("display","none");
            $("#new").css("display","none");
            $("#cart_items").css("display","none");
            $("#section_support").css("display","none");
            $("#sectionProductos").css("display","flex");
        }
        
        function getQueryVariable(variable) {
            var query = window.location.search.substring(1);
            var vars = query.split("&");
            for (var i=0; i < vars.length; i++) {
                var pair = vars[i].split("=");
                if(pair[0] == variable) {
                    return pair[1];
                }
            }
            return false;
        }

        function Mensaje()
        {
            var m = getQueryVariable('m');
            if (m == "ok")
            {
                history.pushState(null, "", "index.php");
                swal({
                    title: "Enhorabuena",
                    text: "Nos estaremos comunicando con usted proximamente",
                    type: "success",
                    closeOnConfirm: true,
                    confirmButtonColor: "#2FDF50"
                });
            }

            if (m == "error")
            {
                swal({
                    title: "Lo sentimos",
                    text: "Hubo un error en el sistema, comuniquese con soporte",
                    type: "error",
                    closeOnConfirm: true,
                    confirmButtonColor: "#2FDF50"
                });
            }
        }

        $(document).ready(function(){$("#btn_soporte").click()});

        window.onload = Mensaje();
        $(document).ready(function () {
            setCategorysMostSold(); 
            setNavBar();
            setAllProductos();
        });

            $(document).ready(function() {


                $("#comprar").click(function(e) {

                    var validar_formulario = $("#form-comprar").validate({
                        rules: {
                            full_name: { required: true},
                            phone_contact: { required: true},
                            email: { required: true},
                            location: { required: true},
                            address: { required: true}
                        },
                        messages: {
                            full_name: "Este campo es requerido",
                            phone_contact: "Este campo es requerido",
                            email: "Este campo es requerido",
                            location: "Este campos es requerido",
                            address: "Este campo es requerido"
                        },
                    });

                    if(validar_formulario.form()){
                        $.ajax({
                            type: 'POST',
                            url: 'http://localhost/gb/gb/ajaxComprar',
                            data: $('#form-comprar').serialize(),
                            dataType : 'json',
                            success: function(data){
                                clearCart();
                                window.location.href="../index.php?m=ok";
                            },
                            error: function(data){
                                console.log("Error");
                            }
                        });
                    }
                });
                $("#media").ready(function(){
                    if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)){
                        $("#media").attr("href","css/mediaMobile.css");  
                    }
                    else{
                        $("#media").attr("href","css/mediaDesktop.css");  
                    }                                          
                });
            });
        </script>
    </footer>
</html>