$(function () {
    /*
    ==============================================================================================================================================
                                                    BEGGIN MOBILE 
    ==============================================================================================================================================
    */
    //calculo el ancho de la pagina
    var ancho = $(window).width(),
        enlaces = $("#enlaces"),
        btnMenu = $("#btn-menu"),
        icono = $("#btn-menu i");

        if (ancho <= 899) {
            enlaces.hide();
            icono.addClass("fa-bars");
        }
        else{
            enlaces.show();
        }

    btnMenu.on("click", function (e) {
        enlaces.slideToggle();
        icono.toggleClass("fa-bars");
        icono.toggleClass("fa-times");
    });

    $(window).on("resize", function () {
        if ($(this).width() >= 900) {
            enlaces.show();
            icono.addClass("fa-times");
            icono.removeClass("fa-bars");
        }
        else {
            enlaces.hide();
            icono.addClass("fa-bars");
            icono.removeClass("fa-times");
            setNavBar();
        }
    });

    function showShop() {
        $("#principal").css("display", "none");
        $("#mostSold").css("display", "none");
        $("#new").css("display", "none");
        $("#cart_items").css("display", "none");
        $("#section_support").css("display", "none");
        $("#sectionProductos").css("display", "block");
    }

    function showCart() {
        $("#principal").css("display", "none");
        $("#mostSold").css("display", "none");
        $("#new").css("display", "none");
        $("#cart_items").css("display", "inline-block");
        $("#infoPersonal").click();
        $("#volver").click();
        $("#close_carrito").click();
        $("#sectionProductos").css("display", "none");
        setUser();
    }

    function ir(id) {
        $('html,body').animate({
            scrollTop: $("#" + id).offset().top
        }, 3000);
    }

    /*
    ==============================================================================================================================================
                                                        END MOBILE 
    ==============================================================================================================================================
    */

    //Oculta los divs principales y hace aparecer todos los productos

    /*Header beggin*/
    $("#tienda").on("click", function () {
        $("#sectionProductos").addClass("spawn");
        setAllProductos();
        $("#btnInicio").click();
        showShop();
    });

    $("#tienda2").on("click", function () {
        $("#sectionProductos").addClass("spawn");
        $("#btn-menu").click();
        setAllProductos();
        showShop();
    });

    $("#show-cat-btn").on("click", function () {
        showCart();
        if ($(window).width() <= 900) {
            $("#btn-menu").click();
        }
    });
    /* Header end */
    /*Bloque medio beggin*/
    $("#btn_main").on("click", function () {
        $("#sectionProductos").addClass("spawn");
        showShop();
        $('html, body').animate({ scrollTop: 0 }, 1500);
    });

    /*Muestra el contenido del link de la primera imagen a la derecha en un escritorio o a la primera imagen luego de scrollear en el movil */
    $("#btn_right_first").on("click", function () {
        showShop();
        if ($(window).width() >= 900) {
            $("#BabyDolls").click();
        }
        else {
            setProductoCategoria("BabyDolls");
        }
        $('html, body').animate({ scrollTop: 0 }, 1500);
    });
    /*Muestra el contenido del link de la segunda imagen a la derecha en un escritorio o a la primera imagen luego de scrollear en el movil */
    $("#btn_right_second").on("click", function () {
        showShop();
        if ($(window).width() >= 900) {
            $("#Bodys").click();
        }
        else {
            setProductoCategoria("Bodys");
        }
        $('html, body').animate({ scrollTop: 0 }, 1500);
    });
    /*Muestra el contenido del link de la seccion new imagen a la derecha en un escritorio o a la primera imagen luego de scrollear en el movil */
    $("#btn_new_prod").on("click", function () {        
        if ($(window).width() >= 900) {
            $("#Conjuntos").click();
            $('html, body').animate({ scrollTop: 0 }, 1500);
        }
        else {
            setProductoCategoria("Conjuntos");
            $('html, body').animate({ scrollTop: 0 }, 1500);
        }
        showShop();        
    });

    /*Bloque medio end */
    //Muestra el formulario de soporte
    $("#support").on("click", function () {
        $("#section_support").css("display", "block");
        ir("section_support");
    });
    //Da click en el btn siguiente en la tabla de los productos a comprar
    $("#siguiente").on("click", function () {
        $("#infoPersonal").click();
    });
});
