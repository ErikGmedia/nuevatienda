function format_miles(x) {
    console.log("format_miles");
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
}

/*==============================================================================
 *                            CATEGORIAS MÁS VENDIDAS
 * =============================================================================
 */

var comillas = '"';

function setCategoriasReco() {

    $.ajax({
        url: 'http://localhost/gb/gb/ajaxGetCategoriasBoutique',
        dataType : 'json',
        success: function(respuesta) {

            var html = "";

            for(var i=0;i<6;i++){
                var nombre_categoria = '"'+respuesta[i]['nombre_categoria']+'"';
                html += "<button class='stext-106 cl6 hov1 bor3 trans-04 m-r-32 m-tb-5' onclick='setProductosReco("+nombre_categoria+")' data-filter='"+respuesta[i]['nombre_categoria']+"'>"+respuesta[i]['nombre_categoria']+"</button>";
            }

            $("#cate_product_recomendados").html("");
            $("#cate_product_recomendados").html(html);
        },
        error: function() {
            console.log("No se ha podido obtener la información");
        }
    });
}
function setNavBar(){
    $.ajax({
          url:'http://localhost/gb/gb/ajaxGetCategorysMostSold',
          type : 'GET',
          datatype:"json",
          success: function(respuesta){

           var categorias = respuesta;
           
           var html="";
            
           var screen=$(document).width();
            
               for(var i=0; i<categorias.length;i++){
                    if(screen>=1024){
                        if(i>0 && i<=4){
                            html+="<a class='nav-item nav-item-generate' href='#' onclick='linkInNav(this);' data-value='"+categorias[i]+"'>"+categorias[i]+"</a>";
                        }
                        else if(i==5){
                        html+="<div class='despliega'><a href='#'>M&aacute;s categorias</a><div class='content_more_main'>"+
                                "<a data-value='"+categorias[i]+"' href='#' class='nav-item nav-item-generate link_more_category_main' onclick='linkInNav(this);'>"+categorias[i]+"</a>";
                        }
                        else if(i>5){
                            html+="<a data-value='"+categorias[i]+"' href='#' class='nav-item nav-item-generate link_more_category_main' onclick='linkInNav(this);'>"+categorias[i]+"</a>";
                        }
                    }
                    else{
                        if(i>0 && i<=3){
                            html+="<a class='nav-item nav-item-generate' href='#' onclick='linkInNav(this);' data-value='"+categorias[i]+"'>"+categorias[i]+"</a>";
                        }else if(i==4){
                            html+="<div class='despliega'><a href='#'>M&aacute;s categorias</a><div class='content_more_main'>"+
                            "<a data-value='"+categorias[i]+"' href='#' class='nav-item nav-item-generate link_more_category_main' onclick='linkInNav(this);'>"+categorias[i]+"</a>";
                        }
                        else if(i>=5){
                            html+="<a data-value='"+categorias[i]+"' href='#' class='nav-item nav-item-generate link_more_category_main' onclick='linkInNav(this);'>"+categorias[i]+"</a>";
                        }
                    }
                }
                html+="</div></div>";
              $("#categoryGenerate").html("");                    
              $("#categoryGenerate").html(html);             
          },

          error: function(){
              console.log("No se ha podido obtener la información");
          }
    });
}
function setCategorysMostSold(){
    $.ajax({
          url:'http://localhost/gb/gb/ajaxGetCategorysMostSold',
          type : 'GET',
          datatype:"json",
          success: function(respuesta){

           var categorias = respuesta;
           
           var html="";
            
           var screen=$(window).width();
            
               for(var i=0; i<categorias.length;i++){
                   if(screen>=900){
                        if(i==0){
                            html+="<div class='btns_categorys' ><button id='btnInicio' class='btnCategory btnsLinks' href='#' onclick='setAllProductos();' >Inicio</button></div>";
                        }
                        else if(i<=3){
                            html+="<div class='btns_categorys' ><button id='"+categorias[i]+"' class='btnCategory btnsLinks' href='#' onclick='setProductoCategoria("+comillas+categorias[i]+comillas+");' >"+categorias[i]+"</button></div>";
                        }
                        else if(i==4){
                            html+="<div class='btns_categorys despliegue' ><label class='label_more_category'>M&Aacute;S CATEGORIAS</label><div class='content_more_categorias'>";
                            html+="<a  id='"+categorias[i]+"' class='linkCategorys btnsLinks' href='#' onclick='setProductoCategoria("+comillas+categorias[i]+comillas+");'>"+categorias[i]+"</a>";                            
                        }
                        else if(i>4){
                            html+="<a  id='"+categorias[i]+"' class='linkCategorys btnsLinks' href='#' onclick='setProductoCategoria("+comillas+categorias[i]+comillas+");'>"+categorias[i]+"</a>";
                        }
                   }
                }
                html+="</div></div>";
              $("#categorys_most_sold").html("");                    
              $("#categorys_most_sold").html(html);             
          },

          error: function(){
              console.log("No se ha podido obtener la información");
          }
    });
}
function setProductosReco(categoria) {
//how-active1
    $.ajax({
        url: 'http://localhost/gb/gb/ajaxGetProCategoriaBoutique',
        data:  { 'categoria' : categoria },
        type : 'POST',
        dataType : 'json',
        success: function(productos) {

            $("#product_home").html("");

            var categoriaButton = $('#cate_product_recomendados button');

            $(categoriaButton).each(function(){
                if($(this).attr('data-filter') ===  categoria){
                    $(this).addClass('how-active1');
                }else{
                    $(this).removeClass('how-active1');
                }

            });

            var list_group = 0;
            var html = "";
            for (var e=0; e < productos.length; e++){
                if(list_group === 0){ html = ""; html += "<div '>"; }
                html += "<div class='product-line "+categoria+"'>\n" +
                    "          <!-- Block2 -->\n" +
                    "          <div class='block2'>\n" +
                    "           <div class='block2-pic hov-img0'>\n" +
                    "            <img src='http://localhost/gb/public/img/img_productos/"+productos[e]['foto']+"' alt='IMG-PRODUCT' style='width: 270px; height: 334.34px' >\n" +
                    "            <button class='block2-btn flex-c-m stext-103 cl2 size-102 bg0 bor2 hov-btn1 p-lr-15 trans-04 js-show-modal1' onclick='modalDetalleProducto("+productos[e]['id']+")'>Vista Rapida</button>\n" +
                    "           </div>\n" +
                    "\n" +
                    "           <div class='block2-txt flex-w flex-t p-t-14'>\n" +
                    "             <div class='block2-txt-child1 flex-col-l'>\n" +
                    "                <a href='product-detail.html' class='stext-104 cl4 hov-cl1 trans-04 js-name-b2 p-b-6'>"+productos[e]['nombre']+"</a>\n" +
                    "\n" +
                    "             <span class='stext-105 cl3'>$"+format_miles(parseFloat(productos[e]['precio_unidad']))+"</span>\n" +
                    "             </div>\n" +
                    "           </div>\n" +
                    "          </div>\n" +
                    "         </div>";

                list_group = list_group + 1;

                if(list_group == 4){
                    list_group = 0;
                    html+="</div>";
                    $("#product_home").append(html);
                }

            }

            if(list_group > 0){
                html+="</div>";
                $("#product_home").append(html);
                

            }
            //paginacion();
        },
        error: function() {
            console.log("No se ha podido obtener la información");
        }
    });
}
function setCategoriasFiltro() {

    $.ajax({
        url: 'http://localhost/gb/gb/ajaxGetCategoriasBoutique',
        dataType : 'json',
        success: function(respuesta) {

            var h = "<div class='filter-col1 p-r-15 p-b-27'>\n" +
                "        <div class='mtext-102 cl2 p-b-15'>\n" +
                "              Categorias\n" +
                "        </div>\n" +
                "\n" +
                "        <ul id='item-categorias'>\n" +
                "\n" +
                "       </ul>\n" +
                " </div>";

            var html = "";
            var nombre_categoria = "";
            var group_cate = 0;
            for(var i=0;i<respuesta.length;i++){
                if(group_cate == 0){ html=""; html = "<div class='filter-col1 p-r-15 p-b-27'><div class='mtext-102 cl2 p-b-15'></div><ul id='item-categorias'>"}
                nombre_categoria = '"'+respuesta[i]['nombre_categoria']+'"';
                if(i === 0){
                    html += "<li class='p-b-6'><button class='filter-link stext-106 trans-04  filter-link-active' data-categoria='"+respuesta[i]['nombre_categoria']+"' onclick='setProductoCategoria("+nombre_categoria+")'>"+respuesta[i]['nombre_categoria']+"</button></li>";
                }else{
                    html += "<li class='p-b-6'><button class='filter-link stext-106 trans-04' data-categoria='"+respuesta[i]['nombre_categoria']+"' onclick='setProductoCategoria("+nombre_categoria+")'>"+respuesta[i]['nombre_categoria']+"</button></li>";
                }

                group_cate = group_cate + 1;

                if(group_cate == 7){
                    group_cate = 0;
                    html+="</ul></div>";
                    $("#filtro_cate").append(html);
                }
            }

            /*$("#item-categorias").html("");
            $("#item-categorias").html(html);*/
        },
        error: function() {
            console.log("No se ha podido obtener la información");
        }
    });

}
function setProductoCategoria(categoria){
    $.ajax({
        url: 'http://localhost/gb/gb/ajaxGetProCategoriaBoutique',
        data:  { 'categoria' : categoria },
        type : 'POST',
        dataType : 'json',
        success: function(productos) {
            //console.log(productos);
            var categoriaButton = $('#item-categorias button');
            $(categoriaButton).each(function(){
                //console.log($(this).attr('data-categoria'));
                if($(this).attr('data-categoria') ===  categoria){
                    $(this).addClass('filter-link-active');
                }else{
                    $(this).removeClass('filter-link-active');
                }

            });

            var list_group = 0;
            var html = "";
            $("#list_product").html("");
            for (var e=0; e < productos.length; e++){
                if(list_group === 0){ html = ""; html += "<div class='product-line'>"; }
                html += "<div class='product col-xs-3' >\n" +
                "  <div class='product-img divList' onclick='modalDetalleProducto("+productos[e]['id']+");'>\n" +
                "      <img class='imgList' style='width:10;' src='http://localhost/gb/public/img/img_productos/"+productos[e]['foto']+"' alt=''>\n" +
                "      <div class='product-label'>\n" +
                // "          <span class='sale'>-30%</span>\n" +
                // "          <span class='new'>Nuevo</span>\n" +
                "      </div>\n" +
                "  </div>\n" +
                "  <div class='product-body'>\n" +
                "      <p class='product-category' >"+productos[e]['nombre']+"</p>\n" +
                "      <h4 class='product-price'>"+format_miles(parseFloat(productos[e]['precio_unidad']))+"</h4>\n" +
                "  </div>\n" +
                "  <div class='add-to-cart '>\n" +
                "      <button class='add-to-cart-btn' onclick='addCart("+productos[e]['id_producto']+","+productos[e]['precio_unidad']+","+comillas+String(productos[e]['nombre'])+comillas+","+productos[e]['codigo']+","+comillas+String(productos[e]['foto'])+comillas+","+productos[e]['cant_dis']+")' ></i> Añadir al carrito</button>\n" +
                "  </div>\n" +
                "</div>";

                list_group = list_group + 1;

                if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)){
                    $cantidadDeProductos=3;
                }
                else{
                    $cantidadDeProductos=5;
                }

                if(list_group == $cantidadDeProductos){
                    list_group = 0;
                    html+="</div>";
                    $("#list_product").append(html);
                    $("#list_product").css("display","block");             
                    $("#tab1").css("display","none");                     
                }

            }

            if(list_group > 0){
                html+="</div>";
                $("#list_product").append(html);
                $("#list_product").css("display","block");               
                $("#tab1").css("display","none");                  
            }
            //paginacion(); 
            $("#title_categoria").html(categoria);
        },
        error: function() {
            console.log("No se ha podido obtener la información");
        }
    });
}
var lastId=0;
var contalo=0;
function modalDetalleProducto(id)
{
    $.ajax({
        url: 'http://localhost/gb/gb/quickView',
        data:  { 'id' : id },
        type : 'POST',
        dataType : 'json',
        success: function(producto) {
            
            var product_iden = producto['id'];
            //console.log("soy el id"+ product_iden);
            var product_unidad = producto['precio_unidad'];
            var product_nombre = '"'+producto['nombre'].replace('"', "")+'"';
            var product_codigo = producto['codigo'];
            var product_foto = '"'+producto['foto']+'"';
            var cant_dis = parseFloat(producto['cant_disponible_bodega']) + parseFloat(producto['cant_disponible_penthouse']);

            $(".js-modal1 #modal_img_detail").attr('src', 'http://localhost/gb/public/img/img_productos/'+producto['foto']);
            $(".js-modal1 #modal_img_expand").attr('href', 'http://localhost/gb/public/img/img_productos/'+producto['foto']);
            $(".js-modal1 #modal_img_thumb").attr('data-thumb', 'http://localhost/gb/public/img/img_productos/'+producto['foto']);
            $(".js-modal1 #modal_img_nombre").html(producto['nombre']);
        if(contalo==0){
            $(".js-modal1 #cart_price_").attr("id","cart_price_"+product_iden);
            $(".js-modal1 #cart_price_"+product_iden).html(format_miles(parseFloat(producto['precio_unidad'])));
            contalo++;
        }
        else{
            $(".js-modal1 #cart_price_"+lastId).attr("id","cart_price_"+product_iden);
            $(".js-modal1 #cart_price_"+product_iden).html(format_miles(parseFloat(producto['precio_unidad'])));
        }

            var html = "<div class='wrap-num-product flex-w m-r-20 m-tb-10'>\n" +
                "                     <div class='btn-num-product-down cl8 hov-btn3 trans-04 flex-c-m cart_quantity_down_"+product_iden+"' onclick='sumCartQuantityDown("+product_iden+")'>\n" +
                "                       <i class='fs-16 zmdi zmdi-minus'></i>\n" +
                "                     </div>\n" +
                "\n" +
                "                     <input class='mtext-104 cl3 txt-center num-product' id='cart_quantity_input_"+producto['id']+"' type='number' name='num-product' data-cantdis='"+cant_dis+"'  value='1'>\n" +
                "\n" +
                "                     <div class='btn-num-product-up cl8 hov-btn3 trans-04 flex-c-m cart_quantity_up_"+product_iden+"' onclick='sumCartQuantityUp("+product_iden+")'>\n" +
                "                        <i class='fs-16 zmdi zmdi-plus'></i>\n" +
                "                     </div>\n" +
                "                    </div>\n" +
                "\n" +
                "                    <button type='button' class='btn_buy_modal flex-c-m stext-101 cl0 size-101 bg1 bor1 hov-btn1 p-lr-15 trans-04 js-addcart-detail' onclick='addCart("+product_iden+","+product_unidad+","+String(product_nombre)+","+product_codigo+","+String(product_foto)+","+cant_dis+")'>\n" +
                "                      Agregar Al Carrito\n" +
                "                    </button>";
            $("#action-addCart").html("");
            $("#action-addCart").html(html);
            $('.js-modal1').addClass('show-modal1');
            console.log("No lo se dime tu");
            lastId=product_iden;
        },
        error: function() {
            console.log("No se ha podido obtener la información");
        }
    });
}
function setAllProductos() {

    //how-active1
    $.ajax({
        url: 'http://localhost/gb/gb/ajaxGetAllProducts',
        type : 'GET',
        dataType : 'json',
        success: function(productos) {
            //console.log(productos);

            $("#tab1").html("");

            //var categoriaButton = $('#cate_product_recomendados button');

            /*$(categoriaButton).each(function(){
                if($(this).attr('data-filter') ===  categoria){
                    $(this).addClass('how-active1');
                }else{
                    $(this).removeClass('how-active1');
                }

            });*/
            $("#tab1").html("");
            var list_group = 0;
            var html = "";
            var cont = 0;
            var comillas = '"';
            var clasCss="class='product-line products-slick tab-pane active clearfix'";
            for (var e=0; e < productos.length; e++){
                var name_product = productos[e]['nombre'].replace('"', "");
                if(list_group === 0){ 
                    html = "";
                    if(cont>2){
                        clasCss="class='product-line products-slick tab-pane active  clearfix'";
                    }
                    html += "<div id='tab1_"+cont+"' "+clasCss+"  data-nav='#slick-nav-1'>"; }
                    html += "<div class='product' >\n" +
                    "  <div class='product-img divList ' onclick='modalDetalleProducto("+productos[e]['id_producto']+");'>\n" +
                    "      <img class='imgList' style='width:10;' src='http://localhost/gb/public/img/img_productos/"+productos[e]['foto']+"' alt=''>\n" +
                    "      <div class='product-label'>\n" +
                    // "          <span class='sale'>-30%</span>\n" +
                    // "          <span class='new'>Nuevo</span>\n" +
                    "      </div>\n" +
                    "  </div>\n" +
                    "  <div class='product-body'>\n" +
                    "      <p class='product-category' >"+productos[e]['nombre']+"</p>\n" +
                    "      <h4 class='product-price'>"+format_miles(parseFloat(productos[e]['precio_unidad']))+"</h4>\n" +
                    "  </div>\n" +
                    "  <div class='add-to-cart '>\n" +
                    "      <button class='add-to-cart-btn' onclick='addCart("+productos[e]['id_producto']+","+productos[e]['precio_unidad']+","+comillas+String(name_product)+comillas+","+productos[e]['codigo']+","+comillas+String(productos[e]['foto'])+comillas+","+productos[e]['cant_dis']+")' ></i> Añadir al carrito</button>\n" +
                    "  </div>\n" +
                    "</div>";

                list_group = list_group + 1;

                if(list_group == 5){
                    list_group = 0;
                    html+="</div>";
                    $("#tab1").append(html);
                    cont++;
                }

            }

            if(list_group > 0){
                html+="</div>";
                $("#tab1").append(html);
            }
            //paginacion();
            $("#tab1").css("display","flex");
        },
        error: function() {
            console.log("No se ha podido obtener la información");
        }
    });
}
function setSearchProduct(cadena){
    if(cadena==""){
        setAllProductos();
    }
    else{
        $.ajax({
            url: 'http://localhost/gb/gb/ajaxGetSearchProducts',
            data:  { 'cadena' : cadena },        
            type : 'POST',
            dataType : 'json',
            success: function(productos) {
                var list_group = 0;
                var html = "";
                $("#list_product").html("");
                for (var e=0; e < productos.length; e++){
                    if(list_group === 0){ html = ""; html += "<div class='product-line'>"; }
                    html += "<div class='product col-xs-3' >\n" +
                    "  <div class='product-img divList' onclick='modalDetalleProducto("+productos[e]['id_producto']+");'>\n" +
                    "      <img class='imgList' style='width:10;' src='http://localhost/gb/public/img/img_productos/"+productos[e]['foto']+"' alt=''>\n" +
                    "      <div class='product-label'>\n" +
                    // "          <span class='sale'>-30%</span>\n" +
                    // "          <span class='new'>Nuevo</span>\n" +
                    "      </div>\n" +
                    "  </div>\n" +
                    "  <div class='product-body'>\n" +
                    "      <p class='product-category' >"+productos[e]['nombre']+"</p>\n" +
                    "      <h4 class='product-price'>"+format_miles(parseFloat(productos[e]['precio_unidad']))+"</h4>\n" +
                    "  </div>\n" +
                    "  <div class='add-to-cart '>\n" +
                    "      <button class='add-to-cart-btn' onclick='addCart("+productos[e]['id_producto']+","+productos[e]['precio_unidad']+","+comillas+String(productos[e]['nombre'])+comillas+","+productos[e]['codigo']+","+comillas+String(productos[e]['foto'])+comillas+","+productos[e]['cant_dis']+")' ></i> Añadir al carrito</button>\n" +
                    "  </div>\n" +
                    "</div>";
    
                    list_group = list_group + 1;
    
                    if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)){
                        $cantidadDeProductos=3;
                    }
                    else{
                        $cantidadDeProductos=5;
                    }
    
                    if(list_group == $cantidadDeProductos){
                        list_group = 0;
                        html+="</div>";
                        $("#list_product").append(html);
                        $("#list_product").css("display","block");             
                        $("#tab1").css("display","none");                     
                    }
    
                }
    
                if(list_group > 0){
                    html+="</div>";
                    $("#list_product").append(html);
                    $("#list_product").css("display","block");               
                    $("#tab1").css("display","none");                  
                }
                //paginacion();  
                $("#principal").css("display", "none");
                $("#mostSold").css("display", "none");
                $("#new").css("display", "none");
                $("#cart_items").css("display", "none");
                $("#section_support").css("display", "none");
                $("#sectionProductos").css("display", "block");
            },
            error: function() {
                console.log("No se ha podido obtener la información");
            }
        });
    }
    
}

(function($) {
	"use strict"

	// Mobile Nav toggle
	$('.menu-toggle > a').on('click', function (e) {
		e.preventDefault();
		$('#responsive-nav').toggleClass('active');
	})

	// Fix cart dropdown from closing
	$('.cart-dropdown').on('click', function (e) {
		e.stopPropagation();
	});

	/////////////////////////////////////////

	// Products Slick
	$('.products-slick').each(function() {
		var $this = $(this),
				$nav = $this.attr('data-nav');

		$this.slick({
			slidesToShow: 4,
			slidesToScroll: 1,
			//autoplay: true,
			infinite: true,
			speed: 300,
			dots: false,
			arrows: true,
			appendArrows: $nav ? $nav : false,
			responsive: [{
	        breakpoint: 991,
	        settings: {
	          slidesToShow: 2,
	          slidesToScroll: 1,
	        }
	      },
	      {
	        breakpoint: 480,
	        settings: {
	          slidesToShow: 1,
	          slidesToScroll: 1,
	        }
	      },
	    ]
		});
	});

	// Products Widget Slick
	$('.products-widget-slick').each(function() {
		var $this = $(this),
				$nav = $this.attr('data-nav');

		$this.slick({
			infinite: true,
			autoplay: true,
			speed: 300,
			dots: false,
			arrows: true,
			appendArrows: $nav ? $nav : false,
		});
	});

	/////////////////////////////////////////
/*
	// Product Main img Slick
	$('#product-main-img').slick({
    infinite: true,
    speed: 300,
    dots: false,
    arrows: true,
    fade: true,
    asNavFor: '#product-imgs',
  });

	// Product imgs Slick
  $('#product-imgs').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    arrows: true,
    centerMode: true,
    focusOnSelect: true,
		centerPadding: 0,
		vertical: true,
    asNavFor: '#product-main-img',
		responsive: [{
        breakpoint: 991,
        settings: {
					vertical: false,
					arrows: false,
					dots: true,
        }
      },
    ]
  });
*/
	// Product img zoom
	var zoomMainProduct = document.getElementById('product-main-img');
	if (zoomMainProduct) {
		$('#product-main-img .product-preview').zoom();
	}

	/////////////////////////////////////////

	// Input number
	$('.input-number').each(function() {
		var $this = $(this),
		$input = $this.find('input[type="number"]'),
		up = $this.find('.qty-up'),
		down = $this.find('.qty-down');

		down.on('click', function () {
			var value = parseInt($input.val()) - 1;
			value = value < 1 ? 1 : value;
			$input.val(value);
			$input.change();
			updatePriceSlider($this , value)
		})

		up.on('click', function () {
			var value = parseInt($input.val()) + 1;
			$input.val(value);
			$input.change();
			updatePriceSlider($this , value)
		})
	});

	var priceInputMax = document.getElementById('price-max'),
			priceInputMin = document.getElementById('price-min');

	/*priceInputMax.addEventListener('change', function(){
		updatePriceSlider($(this).parent() , this.value)
	});

	priceInputMin.addEventListener('change', function(){
		updatePriceSlider($(this).parent() , this.value)
	});*/

	function updatePriceSlider(elem , value) {
		if ( elem.hasClass('price-min') ) {
			//console.log('min')
			priceSlider.noUiSlider.set([value, null]);
		} else if ( elem.hasClass('price-max')) {
			//console.log('max')
			priceSlider.noUiSlider.set([null, value]);
		}
	}

	// Price Slider
	var priceSlider = document.getElementById('price-slider');
	if (priceSlider) {
		noUiSlider.create(priceSlider, {
			start: [1, 999],
			connect: true,
			step: 1,
			range: {
				'min': 1,
				'max': 999
			}
		});

		priceSlider.noUiSlider.on('update', function( values, handle ) {
			var value = values[handle];
			handle ? priceInputMax.value = value : priceInputMin.value = value
		});
	}

    /*==================================================================
    [ Show modal1 ]*/
    $('.js-show-modal1').on('click',function(e){
        e.preventDefault();
        $('.js-modal1').addClass('show-modal1');
    });

    $('.js-hide-modal1').on('click',function(){
        $('.js-modal1').removeClass('show-modal1');
    });

    $('.gallery-lb').each(function() { // the containers for all your galleries

    });  

    

})(jQuery);
