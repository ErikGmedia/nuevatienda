$(document).ready(function () {

    //reload all products
    $(document).on("click", "#homeNav", function () {
        $("#tab1").addClass("getUpList");
        $("#tab1").css("display", "block");
        $("#list_product").css("display", "none");
    });

    // will first fade out the loading animation 
    $("#loader").fadeOut("slow", function () {
        // will fade out the whole DIV that covers the website.
        $("#preloader").delay(300).fadeOut("slow");
        
        //force page scroll position to top at page refresh
        $('html, body').animate({ scrollTop: 0 }, 'normal');
    });

    // for hero content animations 
    $("html").removeClass('cl-preload');
    $("html").addClass('cl-loaded');
});