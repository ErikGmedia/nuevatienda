var indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;
var db = null;
var sum = 0;

function updateTotal() {

    console.log("updateTotal");

    var uuid = new DeviceUUID().get();

    var transaction = db.transaction(['bought'], 'readwrite');
    var objectStore = transaction.objectStore('bought');

    objectStore.openCursor().onsuccess = function(event) {
        const cursor = event.target.result;
        if (cursor) {
            if (cursor.value.machine_id === uuid) {
                const updateData = cursor.value;
                updateData.total = sum;
                const request = cursor.update(updateData);
                request.onsuccess = function() {
                    console.log("Total Update!!");
                    getTotal();
                };
            }

            cursor.continue();
        }

    };

}
function sumTotal(){

    console.log("sumTotal");
    var getResult = null;
    var uuid = new DeviceUUID().get();
    console.log("sumTotal Machine id "+uuid);
    var transaction = db.transaction(['cart'], 'readonly');
    var store = transaction.objectStore('cart');

    var colum = store.index('machine_id');
    var request = colum.getAll(uuid);

    request.onerror = function(e) {
        console.error('sumTotal Error', e.target.error.name);
    };

    request.onsuccess = function(event) {
        getResult = request.result;
        console.log(getResult);
        $("#detail_cart").attr('data-notify',getResult.length);
        $("#detail_cart_mobile").attr('data-notify',getResult.length);
        sum = 0;
        for(var i=0;i<getResult.length;i++){

            sum = parseFloat(sum) + (parseFloat(getResult[i]['product_cant']) * parseFloat(getResult[i]['product_precio']));
        }

        console.log('Sum Total Result'+sum);

        updateTotal();
    };
}
function getTotal(){

    console.log("getTotal");
    var uuid = new DeviceUUID().get();

    var transaction = db.transaction(['bought'], 'readonly');
    var store = transaction.objectStore('bought');

    var colum = store.index('machine_id');
    var request = colum.get(uuid);

    request.onerror = function(e) {
        console.log("Show Total Error");
    };

    request.onsuccess = function (e) {

        if (typeof request.result === 'undefined') {
            console.log("Show Total Does Not Exist");
        } else {
            $( "#total_cart" ).html( "Total $ "+format_miles(parseFloat(request.result.total))+"</i>" );
            console.log("Show Total If There Is");
            if($("#content_product")){
                $("#total_car_content").html(" ");
                $("#total_car_content").html("<b>Total $ "+format_miles(parseFloat(request.result.total))+"</b>" );

            }
        }
    };
}
function addItemTotal(){
    console.log("addItemTotal");
    var transaction_readwrite = db.transaction(['bought'], 'readwrite');
    var store_readwrite = transaction_readwrite.objectStore('bought');

    var uuid = new DeviceUUID().get();
    console.log(uuid + "---");

    var item = {
        machine_id: uuid,
        total: 0,
    };

    var request_readwrite = store_readwrite.add(item);

    request_readwrite.onerror = function(e) {
        console.log('Error Add Total', e.target.error.name);
    };

    request_readwrite.onsuccess = function(e) {
        console.log('Woot! Did it total');
    };
}
function starDB() {

    console.log("starDB");
    dataBase = indexedDB.open("Shop", 1);
    dataBase.onsuccess = function (evt) {
        db = evt.target.result;
        console.log("Open DB");
        countBought();
        sumTotal();
        listProductCart();
        ajaxUser();
        setUser();

    };
    dataBase.onerror = function (evt) {
        console.error("openDb:", evt.target.errorCode);
    };

    dataBase.onupgradeneeded = function (e) {
        console.log("Create DB");
        db = e.target.result;
        active = dataBase.result;

        //Cart
        objectModels = active.createObjectStore("cart", {keyPath: 'id', autoIncrement: true});
        objectModels.createIndex('machine_id', 'machine_id', {unique: false});
        objectModels.createIndex('product_id', 'product_id', {unique: true});
        objectModels.createIndex('product_cant', 'product_cant', {unique: false});
        objectModels.createIndex('product_precio', 'product_precio', {unique: false});
        objectModels.createIndex('product_nombre', 'product_nombre', {unique: false});
        objectModels.createIndex('product_codigo', 'product_codigo', {unique: false});
        objectModels.createIndex('product_foto', 'product_foto', {unique: false});
        objectModels.createIndex('product_cantdis', 'product_cantdis', {unique: false});

        //Bought
        objectModels = active.createObjectStore("bought", {keyPath: 'id', autoIncrement: true});
        objectModels.createIndex('machine_id', 'machine_id', {unique: true});
        objectModels.createIndex('total', 'total', {unique: false});


    };

}
function addItem(produc_id,produc_cant,produc_precio,produc_nombre,produc_codigo,produc_foto,cant_dis) {

    console.log("addItem");
    var transaction_readwrite = db.transaction(['cart'], 'readwrite');
    var store_readwrite = transaction_readwrite.objectStore('cart');

    var uuid = new DeviceUUID().get();
    console.log(uuid + "---");

    var item = {
        machine_id: uuid,
        product_id: parseFloat(produc_id),
        product_cant: parseFloat(produc_cant),
        product_precio: parseFloat(produc_precio),
        product_nombre: produc_nombre,
        product_codigo: produc_codigo,
        product_foto: produc_foto,
        product_cantdis: cant_dis
    };

    var request_readwrite = store_readwrite.add(item);

    request_readwrite.onerror = function(e) {
        console.log('Error', e.target.error.name);
        console.log('Error', e.target.error.name);
        swal({
            title: "¡Atención!",
            text: "El producto seleccionado ya se encuentra en el carrito de compras",
            icon: "warning"
        });
    };

    request_readwrite.onsuccess = function(e) {

        console.log('Woot! Did it');
        listProductCart();
        sumTotal();

        swal({
            title: "¡Atención!",
            text: "El producto se agrego correctamente al carrito de compras",
            icon: "success"
        });

    };

}
function searchItem(index,value){

    console.log("searchItem");
    var getResult = null;

    var transaction = db.transaction(['cart'], 'readonly');
    var store = transaction.objectStore('cart');

    var colum = store.index(index);
    var request = colum.getAll(value);

    request.onerror = function(e) {
        console.error('searchItem Error', e.target.error.name);
    };

    request.onsuccess = function(event) {
        getResult = request.result;
    };

    return getResult;
}
function addCart(id,precio,nombre,codigo,foto,cant_dis){

    var cantidad = 1;

    if($("#cart_quantity_input_"+id).length === 1)
    {
        cantidad = $("#cart_quantity_input_"+id).val();
    }

    addItem(id,cantidad,precio,String(nombre),codigo,String(foto),parseFloat(cant_dis));
}
function paginacion(){

    console.log("paginacion");
    $(".pagination_ul").html("");
    var numberOfItems = $('.page .list-group').length;
    console.log( "ready!"+numberOfItems );

    var numberOfItems = $('.page .list-group').length; // Get total number of the items that should be paginated
    var limitPerPage = 2; // Limit of items per each page
    $('.page .list-group:gt(' + (limitPerPage - 1) + ')').hide(); // Hide all items over page limits (e.g., 5th item, 6th item, etc.)
    var totalPages = Math.round(numberOfItems / limitPerPage); // Get number of pages

    $(".pagination_ul").append("<li class='current-page how-pagination1 active-pagination1' ><a href='javascript:void(0)' class='flex-c-m  trans-04 m-all-7'>1</a></li>"); // Add first page marker

// Loop to insert page number for each sets of items equal to page limit (e.g., limit of 4 with 20 total items = insert 5 pages)
    for (var i = 2; i <= totalPages; i++) {
        console.log("Agrego item");
        $(".pagination_ul").append("<li class='current-page how-pagination1' ><a href='javascript:void(0)' class='flex-c-m trans-04 m-all-7'>" + i + "</a></li>"); // Insert page number into pagination tabs
    }

// Add next button after all the page numbers
    // $(".pagination").append("<li id='next-page'><a href='javascript:void(0)' aria-label=Next><span aria-hidden=true>&raquo;</span></a></li>");

// Function that displays new items based on page number that was clicked
    $(".pagination_ul li.current-page").on("click", function() {
        console.log("Entro");
        // Check if page number that was clicked on is the current page that is being displayed
        if ($(this).hasClass('active-pagination1')) {
            return false; // Return false (i.e., nothing to do, since user clicked on the page number that is already being displayed)
        } else {
            var currentPage = $(this).index();
            console.log("Current Page:"+currentPage);
            // Get the current page number
            $(".pagination_ul li").removeClass('active-pagination1'); // Remove the 'active' class status from the page that is currently being displayed
            $(this).addClass('active-pagination1'); // Add the 'active' class status to the page that was clicked on
            $(".page .list-group").hide(); // Hide all items in loop, this case, all the list groups
            var grandTotal = limitPerPage * currentPage; // Get the total number of items up to the page number that was clicked on
            console.log("Grand Total: "+limitPerPage +"*"+ currentPage);
            console.log("i: "+grandTotal +"-"+ limitPerPage);
            // Loop through total items, selecting a new set of items based on page number
            for (var i = grandTotal - limitPerPage; i < grandTotal; i++) {
                $(".page .list-group:eq(" + i + ")").show(); // Show items from the new page that was selected
            }
        }

    });
}
function listProductCart(){

    console.log("listProductCart");

    var getResult = null;
    var uuid = new DeviceUUID().get();

    var transaction = db.transaction(['cart'], 'readonly');
    var store = transaction.objectStore('cart');

    var colum = store.index('machine_id');
    var request = colum.getAll(uuid);

    request.onerror = function(e) {
        console.error('sumTotal Error', e.target.error.name);
    };

    request.onsuccess = function(event) {
        getResult = request.result;

        if($(".barra_carrito").length){
            console.log('DETAIL CARRITO');
            console.log(request.result);
            setListProductBarra(request.result);
        }

        if($("#content_product").length){
            setListProductCart(request.result);
        }

        if($("#content_product_detail").length){
            setDetailListProductCart(request.result);
        }

    };



}
function setListProductBarra(array){

    console.log("setListProductBarra");

    $(".barra_carrito").html("");

    for(var i=0;i<array.length;i++) {
        var url_foto = "https://gbmediagroup.com/GB/public/img/img_productos/";
        var foto = url_foto+""+array[i]['product_foto'];
        var nombre = array[i]['product_nombre'];
        var cantidad = array[i]['product_cant'];
        var codigo = "Codigo: "+array[i]['product_codigo'];
        var precio = array[i]['product_precio'];
        var total = parseFloat(array[i]['product_cant']) * parseFloat(array[i]['product_precio']);

        var html = "<br><li class='header-cart-item flex-w flex-t m-b-12'>\n" +
            "            <div class='header-cart-item-img' onclick='deletedProductCart("+array[i]['product_id']+")'>\n" +
            "              <img src='"+foto+"' alt='IMG' >\n" +
            "            </div>\n" +
            "\n" +
            "           <div class='header-cart-item-txt p-t-8'>\n" +
            "              <a href='#' class='header-cart-item-name m-b-18 hov-cl1 trans-04'>"+nombre+"</a>\n" +
            "\n" +
            "              <span class='header-cart-item-info'>"+cantidad+" X $"+format_miles(parseFloat(precio))+"</span>\n" +
            "            </div>\n" +
            "       </li>";
        $(".barra_carrito").append(html);
    }
}
function setListProductCart(array){

    console.log('setListProductCart');
    var url_foto = "https://gbmediagroup.com/GB/public/img/img_productos/";

    var html = "<div class=\"table-responsive\">\n" +
        "        <table class='table table-striped'>\n" +
        "        <thead>\n" +
        "        <tr style='text-align: center'>\n" +
        "        <td>Imagen</td>\n" +
        "        <td>Descripción</td>\n" +
        "        <td>Precio</td>\n" +
        "        <td>Cantidad</td>\n" +
        "        <td>Subtotal</td>\n" +
        "        <td></td>\n" +
        "        </tr>\n" +
        "        </thead>\n" +
        "        <tbody>";

    for(var i=0;i<array.length;i++) {

        var foto = url_foto+""+array[i]['product_foto'];
        var product_id = array[i]['product_id'];
        var codigo = "Codigo: "+array[i]['product_codigo'];
        var precio = array[i]['product_precio'];
        var total = parseFloat(array[i]['product_cant']) * parseFloat(array[i]['product_precio']);
        var cantdis = array[i]['product_cantdis'];
        //sum = parseFloat(sum) + parseFloat(array[i]['product_precio']);

        html += "<tr style='text-align: center'>\n" +
            "    <td>\n" +
            "        <a href=''><img id='cart_img' src='"+foto+"' alt='' style='width: 100px;  height: 100px;'></a>\n" +
            "        </td>\n" +
            "        <td>\n" +
            "        <p>"+array[i]['product_nombre']+"</p>\n" +
            "        <p>"+codigo+"</p>\n" +
            "    </td>\n" +
            "    <td>\n" +
            "           $<a id='cart_price_"+array[i]['product_id']+"'>"+format_miles(parseFloat(precio))+"</a>\n" +
            "        </td>\n" +
            "        <td>" +
            "        <div class='wrap-num-product flex-w m-r-20 m-tb-10' style='margin-left: 160px;'>\n" +
            "            <div class='btn-num-product-down cl8 hov-btn3 trans-04 flex-c-m cart_quantity_down_"+product_id+"' onclick='sumCartQuantityDown("+product_id+")'>\n" +
            "                <i class='fs-16 zmdi zmdi-minus'></i>\n" +
            "            </div>\n" +
            "            <input class='mtext-104 cl3 txt-center num-product' id='cart_quantity_input_"+product_id+"' type='number' name='num-product' data-cantdis='"+cantdis+"' value='"+array[i]['product_cant']+"'>\n" +
            "            <div class='btn-num-product-up cl8 hov-btn3 trans-04 flex-c-m cart_quantity_up_"+product_id+"' onclick='sumCartQuantityUp("+product_id+")'>\n" +
            "                <i class='fs-16 zmdi zmdi-plus'></i>\n" +
            "            </div>\n" +
            "         </div>" +
            "    </td>\n" +
            "        <td>\n" +
            "        <p class='cart_total_price' id='cart_total_price_"+array[i]['product_id']+"'>"+format_miles(parseFloat(total))+"</p>\n" +
            "        </td>\n" +
            "        <td>\n" +
            "        <a class='cart_quantity_delete' onclick='deletedProductCart("+array[i]['product_id']+")'><i class='fa fa-times'></i></a>\n" +
            "    </td>\n" +
            "    </tr>";
    }


    html += "</tbody><tfooter><tr><td></td><td></td><td></td><td></td><td></td><td style='text-aling:center;' id='total_car_content'></td></tr></tfooter>\n" +
        "  </table>" +
        "</div>";

    //console.log("ARRAY COMPLETO"+JSON.stringify(array));
    $("#bought").val(JSON.stringify(array));
    $("#content_product").html("");
    $("#content_product").html(html);
}
function setDetailListProductCart(array)
{
    console.log('setDetailListProductCart');
    var url_foto = "https://gbmediagroup.com/GB/public/img/img_productos/";
    var total_buy = 0;
    var html_detail = "<div class=\"table-responsive\">\n" +
        "        <h6 style='text-align: center'>Detail</h6>\n" +
        "        <table class='table table-striped'>\n" +
        "        <thead>\n" +
        "        <tr style='text-align: center'>\n" +
        "        <td>Imagen</td>\n" +
        "        <td>Descripción</td>\n" +
        "        <td>Precio</td>\n" +
        "        <td>Cantidad</td>\n" +
        "        <td>Total</td>\n" +
        "        </tr>\n" +
        "        </thead>\n" +
        "        <tbody>";

    for(var a=0;a<array.length;a++) {

        var foto_detail = url_foto+""+array[a]['product_foto'];
        var codigo_detail = "Codigo: "+array[a]['product_codigo'];
        var precio_detail = array[a]['product_precio'];
        var total_detail = parseFloat(array[a]['product_cant']) * parseFloat(array[a]['product_precio']);

        total_buy = parseFloat(total_buy) + parseFloat(total_detail);

        html_detail += "<tr style='text-align: center'>\n" +
            "    <td>\n" +
            "        <a href=''><img id='cart_img' src='"+foto_detail+"' alt='' style='width: 100px; height: 100px'></a>\n" +
            "        </td>\n" +
            "        <td>\n" +
            "        <p>"+array[a]['product_nombre']+"</p>\n" +
            "        <p>"+codigo_detail+"</p>\n" +
            "    </td>\n" +
            "    <td>\n" +
            "           $<a id='cart_price_"+array[a]['product_id']+"'>"+format_miles(parseFloat(precio_detail))+"</a>\n" +
            "        </td>\n" +
            "        <td>\n" +
            "           <p>"+array[a]['product_cant']+"</p>\n" +
            "        </td>\n" +
            "        <td>\n" +
            "        <p class=\"cart_total_price\" id='cart_total_price_"+array[a]['product_id']+"'>"+format_miles(parseFloat(total_detail))+"</p>\n" +
            "        </td>\n" +
            "    </tr>";

    }

    html_detail += "</tbody>\n" +
        "  </table>";

    html_detail += "<hr><p style='text-align: right'>Total:  $"+format_miles(total_buy)+"</p><br></div>";

    //console.log("ARRAY COMPLETO"+JSON.stringify(array));
    $("#bought").val(JSON.stringify(array));
    $("#content_product_detail").html("");
    $("#content_product_detail").html(html_detail);

}
function sumCartQuantityUp(id)
    {
        /*console.log("sumCartQuantityUp======="+$("#cart_price_"+id).html());
        console.log("soy el id======="+id);*/

        console.log('sumCartQuantityUp');

        var cant = $("#cart_quantity_input_"+id).val();
        var precio = limiparPuntos($("#cart_price_"+id).html());
        var total = 0;
        var cantdis = parseFloat($("#cart_quantity_input_"+id).data('cantdis'));

        cant = parseFloat(cant) + 1;
        //console.log("Cantidad: "+cant);
        if(cant > 0 && cant <= cantdis){
            total = parseFloat(cant) * parseFloat(precio);
            updateCantProduct(cant,id);
            $("#cart_quantity_input_"+id).val(cant);
            $("#cart_total_price_"+id).html(format_miles(parseFloat(total)));
            listProductCart();
        }

        if(cant < 0){
            total = parseFloat(cant) * parseFloat(precio);
            updateCantProduct(cant,id);
            $("#cart_quantity_input_"+id).val(0);
            $("#cart_total_price_"+id).html(format_miles(parseFloat(total)));
            listProductCart();
        }
    }

function sumCartQuantityDown(id)
    {
        console.log("sumCartQuantityDown");
        var cant = $("#cart_quantity_input_"+id).val();
        var precio = limiparPuntos($("#cart_price_"+id).html());
        var total = 0;

        cant = parseFloat(cant) - 1;

        if(cant > 0){
            total = parseFloat(cant) * parseFloat(precio);
            updateCantProduct(cant,id);
            $("#cart_quantity_input_"+id).val(cant);
            $("#cart_total_price_"+id).html(format_miles(total));
            listProductCart();
        }

        if(cant < 0){
            total = parseFloat(cant) * parseFloat(precio);
            updateCantProduct(cant,id);
            $("#cart_quantity_input_"+id).val(0);
            $("#cart_total_price_"+id).html(format_miles(total));
            listProductCart();
        }
}
function updateCantProduct(cant,id){

    console.log("updateCantProduct");
    var uuid = new DeviceUUID().get();

    var transaction = db.transaction(['cart'], 'readwrite');
    var objectStore = transaction.objectStore('cart');

    objectStore.openCursor().onsuccess = function(event) {

        const cursor = event.target.result;
        if (cursor) {
            //console.log("cursor existe");
            if (cursor.value.machine_id === uuid && parseFloat(cursor.value.product_id) === parseFloat(id)) {

                const updateData = cursor.value;
                updateData.product_cant = cant;
                const request = cursor.update(updateData);

                request.onerror = function(e) {
                    console.error('Cant Update Error', e.target.error.name);
                };
                request.onsuccess = function(e) {
                    sumTotal();
                    //console.log("Cant Update!!"+cursor.value.product_id);
                };
            }

            cursor.continue();
        }
    };
}
function format_miles(x) {
    console.log("format_miles");
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
}
function limiparPuntos(numero) {
    console.log("limiparPuntos");
    //console.log(numero);
    return numero.replace(/\./g, "");
}
function deletedProductCart(id){

    console.log("deletedProductCart");
    var uuid = new DeviceUUID().get();

    var transaction = db.transaction(['cart'], 'readwrite');
    var objectStore = transaction.objectStore('cart');

    objectStore.openCursor().onsuccess = function(event) {

        const cursor = event.target.result;
        if (cursor) {
            //console.log("cursor existe");
            if (cursor.value.machine_id === uuid && parseFloat(cursor.value.product_id) === parseFloat(id)) {

                const request = cursor.delete(cursor.value.id);

                request.onerror = function(e) {
                    console.error('Cant Delete Error', e.target.error.name);
                };
                request.onsuccess = function(e) {
                    sumTotal();
                    listProductCart();
                    //console.log("Cant Delete!!");
                };
            }

            cursor.continue();
        }
    };
}
function clearBought(){

    console.log("clearDatabase");
    var transaction_readwrite = db.transaction(['bought'], 'readwrite');
    var store_readwrite = transaction_readwrite.objectStore('bought');

    var request_readwrite = store_readwrite.clear();

    request_readwrite.onerror = function(e) {
        //console.log('Error Clear', e.target.error.name);
    };

    request_readwrite.onsuccess = function(e) {
        addItemTotal();
        //console.log('Cleaned Bought!!');
    };
}
function clearCart(){

    console.log("clearDatabase");
    var transaction_readwrite = db.transaction(['cart'], 'readwrite');
    var store_readwrite = transaction_readwrite.objectStore('cart');

    var request_readwrite = store_readwrite.clear();

    request_readwrite.onerror = function(e) {
        //console.log('Error Clear', e.target.error.name);
    };

    request_readwrite.onsuccess = function(e) {
        clearBought();
        //clearUser();
        //console.log('Cleaned Cart!!');
    };
}

function clearUser(){

    console.log("clearUser");
    var transaction_readwrite = db.transaction(['user'], 'readwrite');
    var store_readwrite = transaction_readwrite.objectStore('user');

    var request_readwrite = store_readwrite.clear();

    request_readwrite.onerror = function(e) {
        //console.log('Error Clear', e.target.error.name);
    };

    request_readwrite.onsuccess = function(e) {

    };
}

function countBought()
{
    console.log('countBought');

    var transaction = db.transaction(['bought'], 'readonly');
    var objectStore = transaction.objectStore('bought');

    var countRequest = objectStore.count();
    countRequest.onsuccess = function() {
        //console.log(countRequest.result);

        if(countRequest.result === 0){
            addItemTotal();
        }else{
            comprobarMachineId();
        }
    };
}
function comprobarMachineId()
{
    console.log("comprobarMachineId");
    var getResult = null;
    var uuid = new DeviceUUID().get();
    var bandera = true;
    var transaction = db.transaction(['bought'], 'readonly');
    var store = transaction.objectStore('bought');

    var colum = store.index('machine_id');
    var request = colum.getAll(uuid);

    request.onerror = function(e) {
        console.error('Check Machine Error', e.target.error.name);
    };

    request.onsuccess = function(event) {

        getResult = request.result;

        for(var i=0;i<getResult.length;i++){

            if(getResult[i]['machine_id'] === uuid){
                bandera = false;
                return false;
            }
        }

        if(bandera)
        {
            clearCart();
        }

    };

}

function quickView(product_id)
{
    var url = $("#url_img").val();
    var url_foto = "";
    $.ajax({
        url: 'quickView',
        data : { 'id' : product_id },
        type : 'POST',
        dataType : 'json',
        success: function(respuesta) {

            var getResult = null;
            var length = 0;
            url_foto = url + respuesta['foto'];
            var product_iden = respuesta['id'];
            var product_unidad = respuesta['precio_unidad'];
            var product_nombre = '"'+respuesta['nombre']+'"';
            var product_codigo = respuesta['codigo'];
            var product_foto = '"'+respuesta['foto']+'"';
            var html = "";

            var transaction = db.transaction(['cart'], 'readonly');
            var store = transaction.objectStore('cart');

            var colum = store.index('product_id');
            var request = colum.getAll(parseFloat(product_iden));

            request.onerror = function(e) {
                console.error('sumTotal Error', e.target.error.name);
            };

            request.onsuccess = function(event) {

                getResult = request.result;
                length = getResult.length;
                if(length === 1)
                {
                    html =  "<div class='col-md-6' style='height: 620px;z-index:2;'> \n" +
                        "               <div class='col-md-12' style='height: 620px; text-align: center'>\n" +
                        "                   <img src='"+url_foto+"' style='height: 480px; margin-top: 55px'/>\n" +
                        "              </div>\n" +
                        "        </div>\n" +
                        "        <div class='col-md-6' style='height: 620px'>\n" +
                        "             <div class='col-md-12' style='height: 310px, margin-bottom: 50px'>\n" +
                        "               <h1><strong>"+respuesta['nombre']+"</strong></h1>\n" +
                        "                  <h2>Codigo: "+respuesta['codigo']+"</h2>\n" +
                        "                    <h3>Precio $ <span id='cart_price_"+product_iden+"'>"+format_miles(parseFloat(respuesta['precio_unidad']))+"</span></h3>\n" +
                        "                     <br>\n" +
                        "               <div class='cart_quantity_button'>\n" +
                        "                   <button class='btn-cart-up cart_quantity_up_"+product_iden+"' onclick='sumCartQuantityUp("+product_iden+")' > + </button>\n" +
                        "                   <input class='cart_quantity_input' id='cart_quantity_input_"+product_iden+"' type='text' name='quantity' value='"+getResult[0]['product_cant']+"' autocomplete='off' size='2'>\n" +
                        "                   <button class='btn-cart-up cart_quantity_down_"+product_iden+"'  onclick='sumCartQuantityDown("+product_iden+")'> - </button>\n" +
                        "               </div>\n" +
                        "             </div>\n" +
                        "              <div class='col-md-12' style='height: 310px'>\n" +
                        "                   <button class='btn-cart-view' type='button' style='width: 100%; margin-top: 50px;' onclick='addCart("+product_iden+","+product_unidad+","+String(product_nombre)+","+product_codigo+","+String(product_foto)+")' ><i class='fa fa-shopping-cart'></i>Add Cart</button>\n" +
                        "              </div>\n" +
                        "        </div>";
                }

                if(length === 0)
                {
                    html =  "<div class='col-md-6' style='height: 620px;z-index:2;'> \n" +
                        "               <div class='col-md-12' style='height: 620px; text-align: center'>\n" +
                        "                   <img src='"+url_foto+"' style='height: 480px; margin-top: 55px'/>\n" +
                        "              </div>\n" +
                        "        </div>\n" +
                        "        <div class='col-md-6' style='height: 620px'>\n" +
                        "             <div class='col-md-12' style='height: 310px, margin-bottom: 50px'>\n" +
                        "               <h1><strong>"+respuesta['nombre']+"</strong></h1>\n" +
                        "                  <h2>Codigo: "+respuesta['codigo']+"</h2>\n" +
                        "                    <h3>Precio $ <span id='cart_price_"+product_iden+"'>"+format_miles(parseFloat(respuesta['precio_unidad']))+"</span></h3>\n" +
                        "                     <br>\n" +
                        "               <div class='cart_quantity_button'>\n" +
                        "                   <button class='btn-cart-up cart_quantity_up_"+product_iden+"' onclick='sumCartQuantityUp("+product_iden+")' > + </button>\n" +
                        "                   <input class='cart_quantity_input' id='cart_quantity_input_"+product_iden+"' type='text' name='quantity' value='0' autocomplete='off' size='2'>\n" +
                        "                   <button class='btn-cart-up cart_quantity_down_"+product_iden+"'  onclick='sumCartQuantityDown("+product_iden+")'> - </button>\n" +
                        "               </div>\n" +
                        "             </div>\n" +
                        "              <div class='col-md-12' style='height: 310px'>\n" +
                        "                   <button class='btn-cart-view' type='button' style='width: 100%; margin-top: 50px;' onclick='addCart("+product_iden+","+product_unidad+","+String(product_nombre)+","+product_codigo+","+String(product_foto)+")' ><i class='fa fa-shopping-cart'></i>Add Cart</button>\n" +
                        "              </div>\n" +
                        "        </div>";
                }

                $("#modal_detail_product").html("");
                $("#modal_detail_product").html(html);

                $('#modal_quickView').modal('show');

            };
        },
        error: function() {
            console.log("No se ha podido obtener la información");
        }
    });

}

function ajaxUser() {

    var user_id = $('#user_id').val();
    var estudio = $('#estudio').val();

    if($('#user_id').val() != "") {
        $.ajax({
            url: 'http://localhost/gb/gb/ajaxUser',
            data: {'user_id': user_id, 'estudio': estudio},
            type: 'POST',
            dataType: 'json',
            success: function (data) {
                console.log(data);
                addItemUser(data);
                history.pushState(null, "", "index.php");

            },
            error: function () {
                console.log("No se ha podido obtener la información");
            }
        });
    }
}

function addItemUser(data){
    console.log("addItemUser");

    window.localStorage.setItem('name_user', data['name_user']);
    window.localStorage.setItem('ciudad', data['ciudad']);
    window.localStorage.setItem('direccion_alternativo', data['direccion_alternativo']);
    window.localStorage.setItem('email', data['email']);
    window.localStorage.setItem('user_id', data['user_id']);
    window.localStorage.setItem('name_user', data['name_user']);
    window.localStorage.setItem('nombre_locacion', data['nombre_locacion']);
    window.localStorage.setItem('nombre_rol', data['nombre_rol']);
    window.localStorage.setItem('telefono_alternativo', data['telefono_alternativo']);
    window.localStorage.setItem('estudio', data['estudio']);

    /*var transaction_readwrite = db.transaction(['user'], 'readwrite');
    var store_readwrite = transaction_readwrite.objectStore('user');

    var uuid = new DeviceUUID().get();
    console.log(uuid + "---");

    var item = {
        machine_id: uuid,
        ciudad: data['ciudad'],
        direccion_alternativo: data['direccion_alternativo'],
        email: data['email'],
        user_id: data['user_id'],
        name_user: data['name_user'],
        nombre_locacion: data['nombre_locacion'],
        nombre_rol: data['nombre_rol'],
        telefono_alternativo: data['telefono_alternativo']

    };

    var request_readwrite = store_readwrite.add(item);

    request_readwrite.onerror = function(e) {
        console.log('Error Add Total', e.target.error.name);
    };

    request_readwrite.onsuccess = function(e) {
        console.log('Woot! Did it total');
    };*/
}

function setUser() {

    console.log("setUser");

    $('#full_name').val(window.localStorage.getItem('name_user'));
    $('#phone_contact').val(window.localStorage.getItem('telefono_alternativo'));
    $('#email').val(window.localStorage.getItem('email'));
    $('#location').val(window.localStorage.getItem('ciudad'));
    $('#address').val(window.localStorage.getItem('direccion_alternativo'));
    $('#rol').val(window.localStorage.getItem('nombre_rol'));
    $('#name_estudio').val(window.localStorage.getItem('estudio'));
    $('#name_locacion').val(window.localStorage.getItem('nombre_locacion'));


/*
    var uuid = new DeviceUUID().get();

    var transaction = db.transaction(['user'], 'readonly');
    var store = transaction.objectStore('user');

    var colum = store.index('machine_id');
    var request = colum.getAll(uuid);

    request.onerror = function(e) {
        console.error('Set User Error', e.target.error.name);
    };

    request.onsuccess = function(event) {
        console.log(request.result);

        if($('#detail_user').length){
            $('#full_name').val(request.result[0]['name_user']);
            $('#phone_contact').val(request.result[0]['telefono_alternativo']);
            $('#email').val(request.result[0]['email']);
            $('#location').val(request.result[0]['ciudad']);
            $('#address').val(request.result[0]['direccion_alternativo']);
            $('#rol').val(request.result[0]['nombre_rol']);
        }
    };
*/
}

    var tabViewCar = 0;

    $("#carrito").on("click", function() {
       $("#show_carrito").addClass("show-header-cart");
    });
    $("#close_carrito").on("click", function() {
        $("#show_carrito").removeClass("show-header-cart");
    });

$(function () {
    starDB();
    //paginacion();
});